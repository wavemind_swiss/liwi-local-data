<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable = [
        'uid',
        'fail_safe',
        'synchronized_at',
        'stage',
        'clinician',
        'nodes',
        'mode',
        'device_id',
        'medical_case_id',
    ];

    /**
     * Get the medical case.
     */
    public function medicalCase()
    {
        return $this->belongsTo(MedicalCase::class);
    }
}
