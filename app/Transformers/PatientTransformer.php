<?php

namespace App\Transformers;

use App\Patient;
use League\Fractal\TransformerAbstract;

class PatientTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include.
     *
     * @var array
     */
    protected $defaultIncludes = [

    ];

    /**
     * List of resources possible to include.
     *
     * @var array
     */
    protected $availableIncludes = [
        'medicalCases',
        'patientValues',
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Patient $patient)
    {
        return [
            'id' => $patient->uuid,
            'fail_safe' => $patient->fail_safe,
            'reason' => $patient->reason,
            'consent_file' => $patient->consent_file,
            'facility' => [
                'uid' => $patient->uid,
                'study_id' => $patient->study_id,
                'group_id' => $patient->group_id,
            ],
            'otherFacility' => [
                'other_uid' => $patient->other_uid,
                'other_study_id' => $patient->other_study_id,
                'other_group_id' => $patient->other_group_id,
            ],
        ];
    }

    /**
     * Include MedicalCases.
     *
     * @param App\Patient $patient
     * @return League\Fractal\CollectionResource
     */
    public function includeMedicalCases(Patient $patient)
    {
        $medicalCases = $patient->medicalCases;
        if (!is_null($medicalCases)) {
            return $this->collection($medicalCases, new MedicalCaseListTransformer);
        }
    }

    /**
     * Include PatientValues.
     *
     * @param App\Patient $patient
     * @return League\Fractal\CollectionResource
     */
    public function includePatientValues(Patient $patient)
    {
        $patientValues = $patient->patientValues;
        if (!is_null($patientValues)) {
            return $this->collection($patientValues, new PatientValueTransformer);
        }
    }
}
