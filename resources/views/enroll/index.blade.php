
<main class="login-form">
  <div class="container">
      <div class="row justify-content-center">
          <div class="col-md-8">
              <div class="card">
                  <div class="card-header"><h3>Enroll your device</h3></div>
                    @if ($is_enrolled ?? '')
                      <div class="card-body">
                        <span>Device is enrolled</span>
                      </div>
                    @endif
                    @if ($error ?? '')
                      <div class="card-body">
                        <span>{{$error ?? ''}}</span>
                      </div>
                    @endif
                    <div class="card-body">
                      <form action="{{ route('enroll.redirect') }}" method="GET">
                          @csrf
                          <div class="col-md-6 offset-md-4">
                              <br>
                              <button type="submit" class="btn btn-primary">
                                  Enroll
                              </button>
                          </div>
                      </form>
                    </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</main>
