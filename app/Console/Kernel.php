<?php

namespace App\Console;

use App\Console\Commands\MedicalCaseClose;
use App\Services\MainDataSynchronization;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        MedicalCaseClose::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('permissions:fix')->hourlyAt(15);
        $schedule->command('version:get')->name('attemptGetVersion')->hourlyAt(40)->withoutOverlapping();
        $schedule->command('medical_case:close')->dailyAt(config('synchronization.daily_close_time'));

        $schedule->call(function () {
            MainDataSynchronization::attemptDailySync();
        })->dailyAt(config('synchronization.daily_sync_time'));

        $schedule->call(function () {
            MainDataSynchronization::attemptImmediateSync();
        })->name('attemptImmediateSync')->everyThirtyMinutes()->withoutOverlapping();

        $schedule->command('auto_diag:send')->dailyAt('12:05');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
