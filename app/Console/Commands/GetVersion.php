<?php

namespace App\Console\Commands;

use App\Version;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class GetVersion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'version:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get last version associated to the facility in Medal-C';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = uniqid();
        Log::info('Checking algorithm version...');

        $last_version = Version::get_current_version();

        $limit = Carbon::now()->subHour(1);

        try {
            $response = Http::timeout(600)->get(env('MEDALC_URL') . '/api/v1/versions/json_from_facility', [
                'health_facility_id' => env('HEALTH_FACILITY_ID'),
                'json_version' => $last_version != null ? $last_version->medal_r_json_version : null,
            ]);
        } catch (ConnectionException $e) {
            Log::info('Network failure while checking algorithm version');

            return 1;
        }

        if ($response->successful()) {
            if ($response->body()) {
                Log::info('Algorithm version retrieved successfully');

                // New version
                if ($last_version == null || ($last_version != null && $last_version->version_medal_c_id != $response['version_id'])) {
                    Version::create([
                        'version_medal_c_id' => $response['version_id'],
                        'medal_r_json_version' => $response['json_version'],
                        'json' => $response,
                        'end_date' => null,
                    ]);

                    if ($last_version != null) {
                        $current_time = Carbon::now();
                        $current_time->toDateTimeString();
                        $last_version->update(['end_date' => $current_time]);
                    }
                } elseif ($last_version != null && $last_version->medal_r_json_version != $response['json_version']) {
                    // Update current version
                    $last_version->update([
                        'medal_r_json_version' => $response['json_version'],
                        'json' => $response,
                    ]);
                }
                Log::info('The database was successfully updated with the new algorithm version');
            } else {
                Log::info('The algorithm is up-to-date, there is nothing to do');
            }
        } else {
            Log::info('Unable to check algorithm version');
        }
    }

    public function allowFetch($last_version)
    {
        $atleast_one_version = Version::all()->count() > 0 ? $atleast_one_version = true : $atleast_one_version = false;
        $time_diff = 0;
        if ($atleast_one_version) {
            $now_Time = Carbon::now();
            $last_version_time = $last_version->updated_at;
            $time_diff = (int) $now_Time->diff($last_version_time)->format('%H');
        }
        if (!$atleast_one_version) {
            return true;
        } elseif ($atleast_one_version && $time_diff > 1) {
            return true;
        } else {
            return false;
        }
    }
}
