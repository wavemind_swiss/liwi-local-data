<?php

namespace Tests\Feature;

use App\MedicalCase;
use App\Patient;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PatientApiTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function testSuccessfullIndex()
    {
        $patient = factory(Patient::class)->make([]);
        $patient->save();

        $medicalCase = factory(MedicalCase::class)->make([
            'patient_id' => $patient->id,
        ]);
        $medicalCase->save();

        // act
        $response = $this->withHeaders([
            'content' => 'application/json',
            'accept' => 'application/json',
        ])->get('/api/patients');

        // assert
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function testSuccessfullGetConsentsFile()
    {
        $patient = factory(Patient::class)->make([]);
        $patient->save();

        $medicalCase = factory(MedicalCase::class)->make([
            'patient_id' => $patient->id,
        ]);
        $medicalCase->save();

        // act
        $response = $this->withHeaders([
            'content' => 'application/json',
            'accept' => 'application/json',
        ])->get('/api/patients/consent_files');

        // assert
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function testSuccessfullFindBy()
    {
        $patient = factory(Patient::class)->make([]);
        $patient->save();

        $medicalCase = factory(MedicalCase::class)->make([
            'patient_id' => $patient->id,
        ]);
        $medicalCase->save();

        // act
        $response = $this->withHeaders([
            'content' => 'application/json',
            'accept' => 'application/json',
        ])->get('/api/patients/find_by/?field=uuid&value=' . $patient->uuid);

        // assert
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function testSuccessfullCreatePatient()
    {
        // prepare
        $data = [
            'patient' => [
                'id' => '7803f0c8-ddc2-4cfa-9a29-a0ee71777b21',
                'uid' => '7ae9c311-e488-4459-8883-0beac80d1b67',
                'first_name' => 'Aubrey',
                'last_name' => 'Tromp',
                'birth_date' => 1586556000000,
                'birth_date_estimated' => false,
                'birth_date_estimated_type' => '',
                'consent' => true,
                'consent_file' => null,
                'createdAt' => 1632316188982,
                'fail_safe' => true,
                'group_id' => '141',
                'other_group_id' => null,
                'other_study_id' => null,
                'other_uid' => null,
                'reason' => null,
                'savedInDatabase' => true,
                'study_id' => '2',
                'updatedAt' => 1632316188982,
                'patientValues' => [
                    0 => [
                        'patient_id' => 'cc4e90a9-4836-4d8f-88fe-ac3be6a3f0d3',
                        'node_id' => 5132,
                        'answer_id' => null,
                        'value' => '529',
                        'fail_safe' => true,
                    ],
                ],
            ],
            'medical_case' => [
                'id' => '477a1af4-4719-4f00-9bd4-39bb5126d2f3',
                'json' => 'test',
                'json_version' => 102,
                'advancement' => [
                    'stage' => 0,
                    'step' => 0,
                ],
                'synchronizedAt' => 0,
                'closedAt' => 1632316545025,
                'createdAt' => 1632316545025,
                'fail_safe' => true,
                'version_id' => 34,
            ],
        ];

        $patient = factory(Patient::class)->make([]);
        $patient->save();

        $medicalCase = factory(MedicalCase::class)->make([
            'patient_id' => $patient->id,
        ]);
        $medicalCase->save();

        // act
        $response = $this->withHeaders([
            'content' => 'application/json',
            'accept' => 'application/json',
        ])->postJson('/api/patients', $data);

        // assert
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function testSuccessfullUpdatePatient()
    {
        // prepare
        $data = [
            'fields' => [
                0 => [
                    'name' => 'birth_date',
                    'value' => 1627654300,
                ],
            ],
        ];
        $patient = factory(Patient::class)->make([]);
        $patient->save();

        // act
        $response = $this->withHeaders([
            'content' => 'application/json',
            'accept' => 'application/json',
        ])->patchJson('/api/patients/' . $patient->uuid, $data);

        // assert
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function testSuccessfullSynchronize()
    {
        // prepare
        $data = [
            'patient' => [
                'id' => '7803f0c8-ddc2-4cfa-9a29-a0ee71777b21',
                'first_name' => 'Jean',
                'last_name' => 'Neymar',
                'birth_date' => 1625225668,
                'birth_date_estimated' => true,
                'birth_date_estimated_type' => 'month',
                'uid' => '7803f0c8-ddc2-4cfa-9a29-a0ee71877b99',
                'study_id' => 'Dynamic TZ',
                'group_id' => '70',
                'other_uid' => '7803f0c8-ddc2-4cfa-9a29-a0ee71777b94',
                'other_study_id' => 'Dynamic TZ',
                'other_group_id' => '70',
                'reason' => 'Because I moved',
                'consent' => true,
                'consent_file' => 'data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==',
                'medical_cases' => [
                    0 => [
                        'id' => '7803f0c8-ddc2-4cfa-9a29-a0ee71877b99',
                        'fail_safe' => false,
                        'json' => [
                            'comment' => 'I love cakes',
                            'consent' => true,
                            'diagnosis' => [
                                'proposed' => [
                                ],
                                'excluded' => [
                                ],
                                'additional' => [
                                ],
                                'agreed' => [
                                ],
                                'refused' => [
                                ],
                                'custom' => [
                                ],
                            ],
                            'nodes' => [
                            ],
                        ],
                        'advancement' => [
                            'stage' => 1,
                            'step' => 1,
                        ],
                        'synchronizedAt' => 1625225668,
                        'closedAt' => 1625225668,
                        'createdAt' => 1625225668,
                        'version_id' => 2,
                        'mac_address' => '00:1B:44:11:3A:B7',
                        'clinician' => 'Juan Nieve',
                    ],
                ],
                'patient_values' => [
                    0 => [
                        'node_id' => 4,
                        'answer_id' => 3,
                        'value' => 'John',
                    ],
                ],
            ],
        ];

        // act
        $response = $this->withHeaders([
            'content' => 'application/json',
            'accept' => 'application/json',
        ])->postJson('/api/patients/synchronize', $data);

        // assert
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function testSucessfullCreatePatientValue()
    {
        // prepare
        $data = [
            'patient_values' => [
                0 => [
                    'uid' => '7803f0c8-ddc2-4cfa-9a29-a0ee71777b36',
                    'node_id' => 4,
                    'answer_id' => null,
                    'value' => null,
                ],
                1 => [
                    'uid' => '7803f0c8-ddc2-4cfa-9a29-a0ee71777b37',
                    'node_id' => 3,
                    'answer_id' => 35,
                    'value' => '26',
                ],
            ],
        ];
        $patient = factory(Patient::class)->make([]);
        $patient->save();

        // act
        $response = $this->withHeaders([
            'content' => 'application/json',
            'accept' => 'application/json',
        ])->postJson('/api/patients/' . $patient->uuid . '/patient_values', $data);

        // assert
        $response->assertStatus(200);
    }
}
