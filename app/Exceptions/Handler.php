<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param \Throwable $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Throwable $exception
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        return parent::render($request, $exception);
    }

    //
    //  /**
    //   * Handle middleware exception
    //   *
    //   * @param \Illuminate\Http\Request $request
    //   * @param \Throwable $exception
    //   * @return response according to status
    //   */
    //  private function handleApiException($request, Throwable $exception)
    //  {
    //    $exception = $this->prepareException($exception);
    //
    //    if ($exception instanceof \Illuminate\Http\Exception\HttpResponseException) {
    //      $exception = $exception->getResponse();
    //    }
    //
    //    if ($exception instanceof \Illuminate\Auth\AuthenticationException) {
    //      $exception = $this->unauthenticated($request, $exception);
    //    }
    //
    //    if ($exception instanceof \Illuminate\Validation\ValidationException) {
    //      $exception = $this->convertValidationExceptionToResponse($exception, $request);
    //    }
    //
    //    return $this->customApiResponse($exception);
    //  }
    //
    //  /**
    //   * Define response format
    //   *
    //   * @param $exception
    //   * @return json response
    //   */
    //  private function customApiResponse($exception)
    //  {
    //    if (method_exists($exception, 'getStatusCode')) {
    //      $statusCode = $exception->getStatusCode();
    //    } else {
    //      $statusCode = 500;
    //    }
    //
    //    $response = [];
    //
    //    switch ($statusCode) {
    //      case 401:
    //        $response['message'] = trans('messages.401');
    //        break;
    //      case 403:
    //        $response['message'] = trans('messages.403');
    //        break;
    //      case 404:
    //        $response['message'] = trans('messages.404');
    //        break;
    //      case 405:
    //        $response['message'] = trans('messages.405');
    //        break;
    //      case 422:
    //        $response['message'] = $exception->original['message'];
    //        $response['errors'] = $exception->original['errors'];
    //        break;
    //      case 429:
    //        $response['message'] = trans('messages.429');
    //        break;
    //      default:
    //        $response['message'] = ($statusCode == 500) ? trans('messages.500') : $exception->getMessage();
    //        break;
    //    }
    //
    //    if (config('app.debug')) {
    //      $response['code'] = $exception->getCode();
    //      $response['message'] = $exception->getMessage();
    //    }
    //
    //    $response['status'] = $statusCode;
    //
    //    return response()->json($response, $statusCode);
    //  }
}
