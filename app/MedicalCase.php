<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicalCase extends Model
{
    /**
     * disable auto update of timestamp column.
     */
    public $timestamps = false;

    protected $fillable = [
        'uuid',
        'version_id',
        'fail_safe',
        'status',
        'json',
        'uuid',
        'clinician',
        'stage',
        'step',
        'device_id',
        'forceClosed',
        'synchronized_at',
        'closed_at',
        'created_at',
        'updated_at',
        'patient_id',
    ];

    protected $casts = [
        'closed_at' => 'datetime:Y-m-d H:i:s',
        'synchronized_at' => 'date:Y-m-d',
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * Get the patient that owns the medical case.
     */
    public function patient()
    {
        return $this->hasOne(Patient::class, 'id', 'patient_id');
    }

    /**
     * Get the medical cases for the patient.
     */
    public function activities()
    {
        return $this->hasMany(Activity::class);
    }

    /**
     * Is user available to open medical case.
     *
     * @param $request
     * @return bool
     */
    public function isLock($request)
    {
        if ($this->clinician == null || $this->clinician == $request->header('x-clinician') && $this->mac_address == $request->header('x-mac-address')) {
            return false;
        }

        return true;
    }

    /**
     *Provides Necessasry information to HF.
     *
     * @return array
     */
    public function getInfo()
    {
        ini_set('memory_limit', '1024M');
        $cases = self::all()->each(function ($case) {
            $case->last_activity = $case->activities->last();
        });
        // for sent cases
        $cases_sent = $cases->filter(function ($case) {
            return $case->synchronized_at != null;
        });
        // for cases not sent
        $cases_not_sent = $cases->filter(function ($case) {
            return $case->synchronized_at == null;
        });

        return [
            'cases_not_sent'=>$cases_not_sent,
            'cases_sent'=>$cases_sent,
        ];
    }
}
