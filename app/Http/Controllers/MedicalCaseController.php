<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Http\Requests\LockMedicalCaseRequest;
use App\Http\Requests\StoreActivitiesMedicalCaseRequest;
use App\Http\Requests\StoreMedicalCaseRequest;
use App\Http\Requests\UpdateMedicalCaseRequest;
use App\Http\Resources\MedicalCaseResource;
use App\Http\Resources\MedicalCaseWithPatientResource;
use App\MedicalCase;
use App\Patient;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class MedicalCaseController extends Controller
{
    public function index(Request $request)
    {
        $params = $request->get('params');
        $terms = null;
        if ($params != null) {
            $params = json_decode($params, true);
            if (array_key_exists('terms', $params)) {
                $terms = $params['terms'];
            }
        }

        $medicalCasesID = MedicalCase::select('medical_cases.id')->leftJoin('patients', 'patients.id', '=', 'medical_cases.patient_id')
            ->where('patients.first_name', 'ILIKE', '%' . $terms . '%')
            ->orWhere('patients.last_name', 'ILIKE', '%' . $terms . '%')->get()->pluck('id');

        try {
            return MedicalCaseWithPatientResource::collection(MedicalCase::whereIn('id', $medicalCasesID)
                ->orderBy('medical_cases.updated_at', 'desc')
                ->paginate(15));
        } catch (QueryException $ex) {
            return response()->json($ex, 422);
        }
    }

    public function findBy(Request $request)
    {
        if (empty($request->field) || $request->field == 'id' || $request->field == 'uid') {
            $field = 'uuid';
        } else {
            $field = $request->field;
        }

        try {
            $medicalCase = MedicalCase::where($field, $request->value)->first();
        } catch (QueryException $ex) {
            return response()->json($ex, 422);
        }
        if ($medicalCase) {
            return new MedicalCaseResource($medicalCase);
        }

        return response()->json(trans('messages.not_found'), 404);
    }

    public function update($uid, UpdateMedicalCaseRequest $request)
    {
        $fields = $request->fields;
        $medicalCase = MedicalCase::firstWhere('uuid', $uid);
        if ($medicalCase) {
            try {
                foreach ($fields as $field) {
                    $name = $field['name'];
                    $value = $field['value'];
                    if ($name == 'closedAt' || $name == 'closed_at') {
                        $name = 'closed_at';
                        if ($value == 0) {
                            $value = null;
                        } else {
                            $value = date('Y-m-d H:i:s', $value / 1000);
                            $value = Carbon::createFromFormat('Y-m-d H:i:s', $value);
                            $medicalCase->status = 'close';
                        }
                    }
                    $medicalCase->{$name} = $value;
                }
                $updatedAt = date('Y-m-d H:i:s', $request->header('localtime') / 1000);
                $medicalCase->updated_at = Carbon::createFromFormat('Y-m-d H:i:s', $updatedAt);
                $medicalCase->save();
            } catch (QueryException $ex) {
                return response()->json($ex, 422);
            }

            return response()->json('Medical case updated', 200);
        }

        return response()->json(trans('messages.not_found'), 404);
    }

    public function lock($uuid, LockMedicalCaseRequest $request)
    {
        $medicalCase = MedicalCase::firstWhere('uuid', $uuid);
        if ($medicalCase) {
            $medicalCase->update([
                'device_id' => $request->header('deviceid'),
                'clinician' => $request->header('clinician'),
            ]);

            return response()->json(trans('messages.lock_success'));
        } else {
            return response()->json(trans('messages.not_found'), 404);
        }
    }

    public function unlock($uuid)
    {
        $medicalCase = MedicalCase::firstWhere('uuid', $uuid);
        if ($medicalCase) {
            $medicalCase->update([
                'clinician' => null,
                'device_id' => null,
            ]);

            return response()->json(trans('messages.unlock_success'));
        } else {
            return response()->json(trans('messages.not_found'), 404);
        }
    }

    public function store($patientID, StoreMedicalCaseRequest $request)
    {
        try {
            $patient = Patient::where('uuid', $patientID)->first();
            if ($patient == null) {
                throw new ModelNotFoundException();
            }
        } catch (ModelNotFoundException $exception) {
            return response()->json(trans('messages.not_found'), 404);
        }

        $closedAt = date('Y-m-d H:i:s', $request->medical_case['closedAt'] / 1000);
        $createdAt = date('Y-m-d H:i:s', $request->medical_case['createdAt'] / 1000);

        MedicalCase::create([
            'uuid' => $request->medical_case['id'],
            'device_id' => $request->header('deviceid'),
            'clinician' => $request->header('clinician'),
            'fail_safe' => $request->medical_case['fail_safe'] ?? false,
            'version_id' => $request->medical_case['version_id'],
            'json' => json_encode($request->medical_case['json']),
            'patient_id' => $patient->id,
            'stage' => $request->medical_case['advancement']['stage'],
            'step' => $request->medical_case['advancement']['step'],
            'status' => ($request->medical_case['closedAt'] == 0) ? '' : 'close',
            'closed_at' => ($request->medical_case['closedAt'] == 0) ? null : Carbon::createFromFormat('Y-m-d H:i:s', $closedAt),
            'created_at' => Carbon::createFromFormat('Y-m-d H:i:s', $createdAt),
            'updated_at' => Carbon::createFromFormat('Y-m-d H:i:s', $createdAt),
        ]);

        return response()->json(trans('messages.new_medical_case_saved'), 200);
    }

    public function storeActivities($uuid, StoreActivitiesMedicalCaseRequest $request)
    {
        try {
            $medicalCase = MedicalCase::where('uuid', $uuid)->first();
            if ($medicalCase == null) {
                return response()->json(trans('messages.not_found'), 404);
            }
        } catch (ModelNotFoundException $exception) {
            return response()->json(trans('messages.not_found'), 404);
        } catch (QueryException $ex) {
            return response()->json($ex, 422);
        }

        foreach ($request->activities as $activity) {
            $createdAt = array_key_exists('createdAt', $activity) ?
                date('Y-m-d H:i:s', $activity['createdAt'] / 1000) :
                now()->format('Y-m-d H:i:s');

            Activity::create([
                'uid' => Str::uuid(),
                'fail_safe' => $activity['fail_safe'] ?? false,
                'stage' => $activity['step'],
                'clinician' => $activity['clinician'],
                'nodes' => json_encode($activity['nodes']),
                'mode' => $activity['mode'] ?? '',
                'device_id' => $activity['device_id'],
                'medical_case_id' => $medicalCase->id,
                'created_at' => Carbon::createFromFormat('Y-m-d H:i:s', $createdAt),
            ]);
        }

        return response()->json('Activities created', 200);
    }

    public function info()
    {
        $cases = new MedicalCase();

        return view('medicalCases.info')->with($cases->getInfo());
    }
}
