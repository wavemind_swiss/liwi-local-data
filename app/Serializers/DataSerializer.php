<?php

namespace App\Serializers;

use League\Fractal\Serializer\ArraySerializer;

class DataSerializer extends ArraySerializer
{
    public function collection($resourceKey, array $data)
    {
        if ($resourceKey) {
            return [$resourceKey => $data];
        }

        if (isset($data[0]) && array_key_exists(0, $data[0])) {
            return $data[0];
        }

        return $data;
    }

    public function item($resourceKey, array $data)
    {
        if ($resourceKey) {
            return [$resourceKey => $data];
        }

        return $data;
    }
}
