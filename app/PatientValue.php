<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientValue extends Model
{
    protected $fillable = [
        'node_id',
        'answer_id',
        'value',
        'patient_id',
        'uuid',
    ];

    /**
     * Get the patient that owns the medical case.
     */
    public function patient()
    {
        return $this->belongsTo('App\Patient');
    }
}
