<?php

namespace Tests\Feature;

use App\MedicalCase;
use App\Patient;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MedicalCaseApiTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function testSuccessfullIndex()
    {
        // act
        $response = $this->withHeaders([
            'content' => 'application/json',
            'accept' => 'application/json',
        ])->get('/api/medical_cases');

        // assert
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function testSuccessfullFindBy()
    {
        $patient = factory(Patient::class)->make([]);
        $patient->save();

        $medicalCase = factory(MedicalCase::class)->make([
            'patient_id' => $patient->id,
        ]);
        $medicalCase->save();

        $response = $this->withHeaders([
            'content' => 'application/json',
            'accept' => 'application/json',
        ])->get('/api/medical_cases/find_by?field=uuid&value=' . $medicalCase->uuid);

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function testSuccessfullLock()
    {
        $patient = factory(Patient::class)->make([]);
        $patient->save();

        $medicalCase = factory(MedicalCase::class)->make([
            'patient_id' => $patient->id,
        ]);
        $medicalCase->save();

        $response = $this->withHeaders([
            'content' => 'application/json',
            'accept' => 'application/json',
        ])->post('/api/medical_cases/' . $medicalCase->uuid . '/unlock');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function testSuccessfullUnlock()
    {
        $patient = factory(Patient::class)->make([]);
        $patient->save();

        $medicalCase = factory(MedicalCase::class)->make([
            'patient_id' => $patient->id,
        ]);
        $medicalCase->save();

        $response = $this->withHeaders([
            'content' => 'application/json',
            'accept' => 'application/json',
        ])->post('/api/medical_cases/' . $medicalCase->uuid . '/lock');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function testSuccessfullCreateMedicalCaseForOnePatient()
    {
        // prepare
        $data = [
            'medical_case' => [
                'id' => '7803f0c8-ddc2-4cfa-9a29-a0ee71877c93',
                'json' => [
                    'comment' => 'I love cakes',
                    'consent' => true,
                    'diagnosis' => [
                        'proposed' => [
                        ],
                        'excluded' => [
                        ],
                        'additional' => [
                        ],
                        'agreed' => [
                        ],
                        'refused' => [
                        ],
                        'custom' => [
                        ],
                    ],
                    'nodes' => [
                    ],
                ],
                'advancement' => [
                    'stage' => 1,
                    'step' => 1,
                ],
                'synchronizedAt' => null,
                'closedAt' => 1365631200000,
                'createdAt' => 1365631200000,
                'version_id' => 2,
            ],
        ];
        $patient = factory(Patient::class)->make([]);
        $patient->save();

        // act
        $response = $this->withHeaders([
            'content' => 'application/json',
            'accept' => 'application/json',
        ])->postJson('/api/patients/' . $patient->uuid . '/medical_cases', $data);

        // assert
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function testSuccessfullUpdateMedicalCase()
    {
        // prepare
        $data = [
            'fields' => [
                0 => [
                    'name' => 'stage',
                    'value' => 'nouveau stage',
                ],
            ],
        ];
        $patient = factory(Patient::class)->make([]);
        $patient->save();

        $medicalCase = factory(MedicalCase::class)->make([
            'patient_id' => $patient->id,
        ]);
        $medicalCase->save();

        // act
        $response = $this->withHeaders([
            'content' => 'application/json',
            'accept' => 'application/json',
        ])->patchJson('/api/medical_cases/' . $medicalCase->uuid, $data);

        // assert
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function testSuccessfullUpdateActivities()
    {
        $data = [
            'activities' => [
                0 => [
                    'step' => 4,
                    'clinician' => 'test',
                    'nodes' => [
                        0 => [
                            'node_id' => 2,
                            'previousValue' => 37,
                            'newValue' => 40,
                        ],
                    ],
                    'mac_address' => 'test',
                ],
                1 => [
                    'step' => 4,
                    'clinician' => 'test',
                    'nodes' => [
                        0 => [
                            'node_id' => 2,
                            'previousValue' => 37,
                            'newValue' => 40,
                        ],
                    ],
                    'mac_address' => 'test',
                ],
            ],
        ];

        $patient = factory(Patient::class)->make([]);
        $patient->save();

        $medicalCase = factory(MedicalCase::class)->make([
            'patient_id' => $patient->id,
        ]);
        $medicalCase->save();

        // act
        $response = $this->withHeaders([
            'content' => 'application/json',
            'accept' => 'application/json',
        ])->postJson('/api/medical_cases/' . $medicalCase->uuid . '/activities', $data);

        // assert
        $response->assertStatus(200);
    }
}
