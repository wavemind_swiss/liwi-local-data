<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $fillable = [
        'uuid',
        'stickers_uuid',
        'first_name',
        'last_name',
        'birth_date',
        'birth_date_estimated',
        'birth_date_estimated_type',
        'consent',
        'fail_safe',
        'uid',
        'study_id', 'group_id',
        'other_uid',
        'other_study_id',
        'other_group_id',
        'reason',
        'consent_file',
        'created_at',
    ];

    protected $casts = [
        'birth_date' => 'date:Y-m-d H:i:s',
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * Get the medical cases for the patient.
     */
    public function medicalCases()
    {
        return $this->hasMany(MedicalCase::class);
    }

    /**
     * Get the patient values for the patient.
     */
    public function patientValues()
    {
        return $this->hasMany(PatientValue::class);
    }
}
