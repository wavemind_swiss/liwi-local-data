<?php

namespace App\Providers;

use App\OauthToken;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Two\InvalidStateException;
use Laravel\Socialite\Two\ProviderInterface;
use Session;

class MedalDataSocialiteProvider extends AbstractProvider implements ProviderInterface
{
    protected $usesPKCE = true;

    /**
     * @inheritdoc
     */
    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase(config('synchronization.sync_domain') . '/' . config('services.medaldata.authorize_uri'), $state);
    }

    /**
     * @inheritdoc
     */
    protected function getTokenUrl()
    {
        return config('synchronization.sync_domain') . '/' . config('services.medaldata.user_token_uri');
    }

    /**
     * Get the access token response for the given code.
     *
     * @param  string  $code
     * @return array
     */
    public function getAccessTokenResponse($code)
    {
        $response = $this->getHttpClient()->post($this->getTokenUrl(), [
            'headers' => ['Accept' => 'application/json'],
            'form_params' => $this->getTokenFields($code),
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * Get the POST fields for the token request.
     *
     * @param  string  $code
     * @return array
     */
    protected function getTokenFields($code)
    {
        $fields = [
            'grant_type' => 'authorization_code',
            'client_id' => Config::get('services.medaldata.client_id'),
            'client_secret' => $this->clientSecret,
            'code' => $code,
            'redirect_uri' => $this->redirectUrl,
        ];

        if ($this->usesPKCE()) {
            $fields['code_verifier'] = $this->request->session()->pull('code_verifier');
        }

        return $fields;
    }

    /**
     * Get the access token response for the given code.
     *
     * @param  string  $code
     * @return array
     */
    public function getAccessRefreshTokenResponse($oauth_token)
    {
        $response = $this->getHttpClient()->post($this->getTokenUrl(), [
            'headers' => ['Accept' => 'application/json'],
            'form_params' => $this->getRefreshTokenFields($oauth_token),
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * Get the POST fields for the refresh token request.
     *
     * @param  string  $code
     * @return array
     */
    protected function getRefreshTokenFields($oauth_token)
    {
        $fields = [
            'grant_type' => 'refresh_token',
            'client_id' => Config::get('services.medaldata.client_id'),
            'refresh_token' => $oauth_token->refresh_token,
            'client_secret' => $this->clientSecret,
            'redirect_uri' => $this->redirectUrl,
        ];

        if ($this->usesPKCE()) {
            $fields['code_verifier'] = Session::get('code_verifier');
        }

        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function getAccessToken()
    {
        if ($this->hasInvalidState()) {
            throw new InvalidStateException;
        }

        $response = $this->getAccessTokenResponse($this->getCode());

        $oauth_token = OauthToken::updateOrCreate(
            ['client_id' => Config::get('services.medaldata.client_id')],
            [
                'access_token' => Arr::get($response, 'access_token'),
                'refresh_token' => Arr::get($response, 'refresh_token'),
                'expires_in' => Arr::get($response, 'expires_in'),
            ]
        );

        return $oauth_token;
    }

    /**
     * @inheritdoc
     */
    public function refreshToken($oauth_token)
    {
        $response = $this->getAccessRefreshTokenResponse($oauth_token);

        $oauth_token->update([
            'access_token' => Arr::get($response, 'access_token'),
            'refresh_token' => Arr::get($response, 'refresh_token'),
            'expires_in' => Arr::get($response, 'expires_in'),
        ]);

        return $oauth_token;
    }

    /**
     * Route to try the Bearer token.
     *
     * @return bool
     */
    public function isTokenValid($oauth_token): bool
    {
        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $oauth_token->access_token,
        ])->post(config('synchronization.sync_domain') . '/api/v1/tokeninfo');

        if ($response->failed() || $response->clientError()) {
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    protected function formatScopes(array $scopes, $scopeSeparator)
    {
    }

    /**
     * @inheritdoc
     */
    protected function mapUserToObject(array $user)
    {
    }

    /**
     * @inheritdoc
     */
    protected function getUserByToken($token)
    {
    }
}
