<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PatientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        date_default_timezone_set('UTC');

        return [
            'id' => $this->uuid,
            'uid' => $this->stickers_uuid,
            'study_id' => $this->study_id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'birth_date' => $this->birth_date->getPreciseTimestamp(3),
            'birth_date_estimated' => $this->birth_date_estimated,
            'birth_date_estimated_type' => $this->birth_date_estimated_type,
            'created_at' => ($this->created_at) ? $this->created_at->getPreciseTimestamp(3) : 0,
            'updated_at' => ($this->updated_at) ? $this->updated_at->getPreciseTimestamp(3) : 0,
            'fail_safe' => $this->fail_safe,
            'medical_cases' => MedicalCaseResource::collection($this->medicalCases),
        ];
    }
}
