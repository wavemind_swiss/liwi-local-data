<?php

namespace App\Services;

use App\Classes\LogViewer;
use App\MedicalCase;
use App\OauthToken;
use App\Patient;
use App\Services\MainDataSynchronization;
use Exception;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;

class AutoDiagnosticService
{
    /**
     * generateDiagnostic.
     *
     * @return void
     */
    public function generateDiagnostic()
    {
        if (!MainDataSynchronization::serverReachable()) {
            Log::info('Server is not reachable,attempt to send auto Diag halted');

            return null;
        }

        Log::info('Server is reachable, attempting to communicate');

        return $this->sendAutoDiag([
            'auth_id' => config('services.medaldata.client_id'),
            'getErrorInLog' => $this->getErrorInLog(),
            'getGitInformation' => $this->getGitInformation(),
            'getSyncInformation' => $this->getSyncInformation(),
            'getDateOfLastPatientCreated' => $this->getDateOfLastPatientCreated(),
        ]);
    }

    /**
     * getErrorInLog.
     *
     * @return array
     */
    private function getErrorInLog()
    {
        try {
            $errorLines = LogViewer::all();

            return $this->createResponse($errorLines);
        } catch (Exception $e) {
            return $this->createResponse($e, false);
        }
    }

    /**
     * getGitInformation.
     *
     * @return void
     */
    private function getGitInformation()
    {
        try {
            $data['commit_hash'] = trim(shell_exec('git rev-parse HEAD'));
            $data['git_branch'] = trim(shell_exec('git rev-parse --abbrev-ref HEAD'));

            return $this->createResponse($data);
        } catch (Exception $e) {
            return $this->createResponse($e, false);
        }
    }

    /**
     * getSyncInformation.
     *
     * @return void
     */
    private function getSyncInformation()
    {
        try {
            $cases = MedicalCase::where('created_at', MedicalCase::max('created_at'));
            $data['case_count'] = $cases->count();
            $data['patient_count'] = $cases->with('patients')->count();
            $data['case_close'] = $cases->where('status', 'close')->count();
            $data['synchronized_false'] = $cases->where('synchronized_at', null)->count();

            return $this->createResponse($data);
        } catch (Exception $e) {
            return $this->createResponse($e, false);
        }
    }

    /**
     * getDateOfLastPatientCreated.
     *
     * @return void
     */
    private function getDateOfLastPatientCreated()
    {
        try {
            $last_patient = Patient::orderby('created_at', 'DESC')->first();
            $last_patient_date['date'] = $last_patient->created_at ?? '';

            return $this->createResponse($last_patient_date);
        } catch (\ErrorException $e) {
            return $this->createResponse($e, false);
        }
    }

    /**
     * sendAutoDiag.
     *
     * @param  mixed $json_data
     * @return void
     */
    private function sendAutoDiag($json_data)
    {
        $oauth_token = OauthToken::where('client_id', config('services.medaldata.client_id'))->first();

        if (!$oauth_token || !$oauth_token->access_token) {
            Log::info('Device not enrolled');

            return null;
        }

        if ($oauth_token->hasExpired()) {
            try {
                $oauth_token = Socialite::driver('medaldata')->refreshToken($oauth_token);
            } catch (Exception $e) {
                Log::info('Refresh token expired');
                report($e);

                return null;
            }
        }

        try {
            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $oauth_token->access_token])
                ->post(config('synchronization.sync_domain') . '/api/v1/sync_diagnostics', $json_data);

            if (!$response->successful()) {
                Log::error('Auto Diag Upload failed with HTTP response code ' . $response->status());

                return null;
            }

            Log::info('Auto Diag Upload successful!');
        } catch (Exception $e) {
            Log::error('Auto Diag Error: ' . $e->getMessage());

            return null;
        }
    }

    /**
     * createResponse.
     *
     * @param  mixed $data
     * @param  mixed $isOkay
     * @return void
     */
    private function createResponse($data, $isOkay = true): array
    {
        if (!$isOkay) {
            $response['status'] = false;
            $response['data'] = ['errorMessage' => $data->getMessage()];

            return $response;
        }
        $response['status'] = true;
        $response['data'] = $data;

        return $response;
    }
}
