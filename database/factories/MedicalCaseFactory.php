<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\MedicalCase;
use Faker\Generator as Faker;

$factory->define(MedicalCase::class, function (Faker $faker) {
    return [
        'uuid' => $faker->uuid,
        'version_id' => $faker->randomNumber(),
        'fail_safe' => $faker->boolean,
        'status' => $faker->randomElement(['close', '']),
        'clinician' => $faker->name,
        'stage' => $faker->randomElement(['', '']),
        'step' => $faker->randomElement(['']),
        'json' => json_encode(''),
        'mac_address' => $faker->macAddress,
        'forceClosed' => $faker->boolean,
        'synchronized_at' => $faker->dateTime,
        'closed_at' => $faker->dateTime,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'patient_id' => null,
    ];
});
