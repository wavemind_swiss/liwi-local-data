<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SyncronizePatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'patient.id' => ['required', 'distinct'],
            'patient.first_name' => ['required'],
            'patient.last_name' => ['required'],
            'patient.birth_date' => ['required'],
            'patient.birth_date_estimated' => ['required'],
            'patient.uid' => ['required', 'distinct'],
            'patient.study_id' => ['required'],
            'patient.group_id' => ['required'],
            'patient.other_uid' => [],
            'patient.other_study_id' => [],
            'patient.other_group_id' => [],
            'patient.reason' => ['nullable', 'present'],
            'patient.consent' => ['required'],

            'patient.medical_cases' => ['required', 'array'],
            'patient.medical_cases.*.id' => ['required', 'distinct'],
            'patient.medical_cases.*.json' => ['required'],
            'patient.medical_cases.*.advancement.stage' => ['required'],
            'patient.medical_cases.*.advancement.step' => ['required'],
            'patient.medical_cases.*.closedAt' => ['required'],
            'patient.medical_cases.*.createdAt' => ['required'],
            'patient.medical_cases.*.version_id' => ['required'],

            'patient.patient_values' => ['present', 'array'],
            'patient.patient_values.*.node_id' => ['required'],
            'patient_values.*.answer_id' => ['nullable', 'present'],
            'patient_values.*.value' => ['nullable', 'present'],
        ];
    }
}
