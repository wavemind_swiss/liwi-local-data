<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('medical_cases', 'MedicalCaseController@index');
Route::get('medical_cases/find_by', 'MedicalCaseController@findBy');
Route::post('medical_cases/{uuid}/unlock', 'MedicalCaseController@unlock');
Route::post('medical_cases/{uuid}/lock', 'MedicalCaseController@lock');
Route::post('/patients/{patientID}/medical_cases', 'MedicalCaseController@store');
Route::patch('medical_cases/{uuid}', 'MedicalCaseController@update');
Route::post('medical_cases/{uuid}/activities', 'MedicalCaseController@storeActivities');

Route::get('patients', 'PatientController@index');
Route::patch('patients/{uid}', 'PatientController@update');
Route::get('patients/consent_files', 'PatientController@consentsFile');
Route::get('patients/find_by', 'PatientController@findBy');
Route::post('patients', 'PatientController@store');
Route::post('patients/synchronize', 'PatientController@synchronize');
Route::post('/patients/{patientID}/patient_values', 'PatientController@storePatientValues');
