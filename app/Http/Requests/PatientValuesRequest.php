<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatientValuesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'patient_values' => ['required', 'array'],
            'patient_values.*.node_id' => ['required'],
            'patient_values.*.answer_id' => ['nullable', 'present'],
            'patient_values.*.value' => ['nullable', 'present'],
        ];
    }
}
