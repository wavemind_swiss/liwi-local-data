<?php

namespace App\Console\Commands;

use App\Services\AutoDiagnosticService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendAutoDiagnostics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auto_diag:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will gather pertinent information and send it to medAL-data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info('auto diagnostic started...');

        return (new AutoDiagnosticService())->generateDiagnostic();
    }
}
