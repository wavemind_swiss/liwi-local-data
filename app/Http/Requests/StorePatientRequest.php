<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'patient.id' => 'required|unique:patients,uuid',
            'patient.uid' => 'required|unique:patients,stickers_uuid',
            'patient.first_name' => 'required',
            'patient.last_name' => 'required',
            'patient.birth_date' => 'required',
            'patient.birth_date_estimated' => 'required',
            'patient.consent' => 'required',
            'patient.group_id' => 'required',
            'patient.other_group_id' => 'present',
            'patient.other_study_id' => 'present',
            'patient.other_uid' => 'present',
            'patient.reason' => 'present',
            'patient.study_id' => 'required',

            'medical_case.id' => 'required|unique:medical_cases,uuid',
            'medical_case.json' => 'required',
            'medical_case.closedAt' => 'required',
            'medical_case.createdAt' => 'required',
            'medical_case.advancement.stage' => 'required',
            'medical_case.advancement.step' => 'required',
            'medical_case.version_id' => 'required',

        ];
    }

    public function messages()
    {
        return [

        ];
    }
}
