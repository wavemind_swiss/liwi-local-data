<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Version extends Model
{
    protected $fillable = [
        'version_medal_c_id',
        'medal_r_json_version',
        'json',
        'end_date',
    ];

    /**
     * Get the current version.
     */
    public static function get_current_version()
    {
        return self::firstWhere('end_date', null);
    }
}
