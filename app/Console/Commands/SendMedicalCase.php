<?php

namespace App\Console\Commands;

use App\MedicalCase;
use App\Services\MainDataSynchronization;
use Illuminate\Console\Command;

class SendMedicalCase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'medical_cases:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $medicalCasesToSynchronize = MedicalCase::where('status', '=', 'close')->where('synchronized_at', '=', null)->get();
        if ($medicalCasesToSynchronize->count() > 0) {
            MainDataSynchronization::sendCases();
        }
    }
}
