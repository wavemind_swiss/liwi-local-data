<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MedicalCaseWithPatientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->uuid,
            'json' => $this->json,
            'advancement' => [
                'stage' => $this->stage,
                'step' => $this->step,
            ],
            'synchronizedAt' => ($this->synchronized_at) ? $this->synchronized_at->getPreciseTimestamp(3) : 0,
            'closedAt' => ($this->closed_at) ? $this->closed_at->getPreciseTimestamp(3) : 0,
            'device_id' => $this->device_id,
            'clinician' => $this->clinician,
            'version_id' => $this->version_id,
            'created_at' => ($this->created_at) ? $this->created_at->getPreciseTimestamp(3) : 0,
            'updated_at' => ($this->updated_at) ? $this->updated_at->getPreciseTimestamp(3) : 0,
            'fail_safe' => $this->fail_safe,
            'forceClosed' => $this->forceClosed,
            'patient' => new PatientResource($this->patient),
        ];
    }
}
