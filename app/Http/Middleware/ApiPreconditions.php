<?php

namespace App\Http\Middleware;

use Closure;

class ApiPreconditions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $acceptHeader = $request->header('Accept');
        if ($acceptHeader != 'application/json') {
            return response()->json('API is only willing to format data as application/json', 406);
        }

//      $ContentTypeHeader = $request->header('Content-type');
//      if ($ContentTypeHeader != 'application/json') {
//        return response()->json("API is only accept content type data as application/json", 406);
//      }
        return $next($request);
    }
}
