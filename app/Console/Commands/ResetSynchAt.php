<?php

namespace App\Console\Commands;

use App\MedicalCase;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ResetSynchAt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'medicalCases:resetSynchAt';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $allMedicalCases = MedicalCase::all();

        foreach ($allMedicalCases as $medicalCase) {
            $medicalCase->synchronized_at = null;
            $medicalCase->save();
        }
        Log::info('All medical cases synchronized date reset');
    }
}
