<?php

namespace App\Console\Commands;

use App\MedicalCase;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class MedicalCaseClose extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'medical_case:close';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Closes medical cases that\'s been open for 12 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $medicalCasesWithoutUpdatedAt = MedicalCase::where('updated_at', null)->get();

        foreach ($medicalCasesWithoutUpdatedAt as $mc) {
            $mc->updated_at = $mc->created_at ?? Carbon::now();
            $mc->save();
            Log::info("Medical case $mc->id with null updated_at value has been updated");
        }

        $limit = Carbon::now()->subHour(12);
        $inactiveMedicalCases = MedicalCase::where('status', '!=', 'close')->where('updated_at', '<', $limit)->get();

        foreach ($inactiveMedicalCases as $medicalCase) {
            $medicalCase->closed_at = $medicalCase->updated_at->add(new \DateInterval('PT12H'));
            $medicalCase->status = 'close';
            $medicalCase->forceClosed = true;
            $medicalCase->save();
        }
    }
}
