<?php

use Illuminate\Support\Facades\Route;

//should be deleted.Now its just for testing

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'IndexController@success')->name('index');

Route::get('/enroll', 'EnrollementController@index')->name('enroll.index');
Route::get('/enroll/redirect', 'EnrollementController@redirectToMedalData')->name('enroll.redirect');
Route::get('/enroll/callback', 'EnrollementController@medalDataCallback')->name('enroll.callback');
Route::get('/info', 'MedicalCaseController@info');
