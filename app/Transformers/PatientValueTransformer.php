<?php

namespace App\Transformers;

use App\PatientValue;
use League\Fractal\TransformerAbstract;

class PatientValueTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include.
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include.
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(PatientValue $patientValue)
    {
        return [
            'id' => $patientValue->id,
            'patient_id' => $patientValue->patient_id,
            'node_id' => $patientValue->node_id,
            'answer_id' => $patientValue->answer_id,
            'value' => $patientValue->value,
        ];
    }
}
