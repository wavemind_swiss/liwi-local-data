<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Patient;
use Faker\Generator as Faker;
use Illuminate\Support\Str as Str;

$factory->define(Patient::class, function (Faker $faker) {
    return [
        'uuid' => Str::uuid(),
        'stickers_uuid' => Str::uuid(),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'birth_date' => $faker->date('Y-m-d'),
        'birth_date_estimated' => $faker->boolean,
        'birth_date_estimated_type' => '',
        'consent' => $faker->boolean,
        'fail_safe' => $faker->boolean,
        'study_id' => $faker->randomNumber(),
        'group_id' => $faker->randomNumber(),
        'other_uid' => Str::uuid(),
        'other_study_id' => $faker->randomNumber(),
        'other_group_id' => $faker->randomNumber(),
        'reason' => $faker->text,
        'consent_file' => $faker->text,
    ];
});
