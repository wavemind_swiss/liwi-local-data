<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreActivitiesMedicalCaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'activities' => ['required', 'array'],
            'activities.*.step' => ['required'],
            'activities.*.clinician' => ['required'],
            'activities.*.nodes' => ['required'],
            'activities.*.device_id' => ['required'],
        ];
    }
}
