<?php

namespace App\Transformers;

use App\MedicalCase;
use App\Version;
use League\Fractal\TransformerAbstract;

class MedicalCaseListTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include.
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include.
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(MedicalCase $medicalCase)
    {
        $medicalCaseJSON = json_decode($medicalCase->case);
        $medicalCaseHash = [
            'id' => $medicalCase->uuid,
            'status' => $medicalCase->status,
            'clinician' => $medicalCase->clinician,
            'mac_address' => $medicalCase->mac_address,
            'values' => [],
        ];

        $algorithm_json = json_decode(Version::get_current_version()->json);

        foreach ($algorithm_json->mobile_config->medical_case_list as $nodeId) {
            $node = $medicalCaseJSON->nodes->$nodeId;
            $node_from_algo = $algorithm_json->nodes->$nodeId;
            if ($node_from_algo->display_format == 'Date') {
                $date = new DateTime($node->value);
                $value = $date->format('d/m/Y');
            } else {
                $value = $node->value;
            }
            array_push($medicalCaseHash['values'], $value);
        }

        return $medicalCaseHash;
    }
}
