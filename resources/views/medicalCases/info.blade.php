<html>
    <head>
        <title>Hub Info</title>
        <link rel="stylesheet" href="{{ asset('custom.css') }}">
        <script src="{{ asset('custom.js') }}" defer></script>
    </head>
    <body>
        <h1>DYNAMIC Health Facility</h1>
        <div class="tab">
            <button class="tablinks" onclick="openCity(event, 'Cases_Sent')" id="defaultOpen"><b>Medical Cases Sent</b></button>
            <button class="tablinks" onclick="openCity(event, 'Cases_Not_Sent')"><b>Cases Not Sent</b></button>
          </div>
          <!-- Tab content -->
          <div id="Cases_Sent" class="tabcontent">
          @if(count($cases_sent)>0)
            <h2>CASES SENT</h2>
                <table>
                <tr>
                  <th>Sn</th>
                  <th>Patient Id</th>
                  <th>Consultation Date</th>
                  <th>Date Sent</th>
                  <th>Forced Closed</th>
                </tr>
                @foreach($cases_sent as $case)
                    <tr>
                      <td scope="row">{{ $loop->index+1 }}</td>
                      <td>{{$case->patient->uuid}}</td>
                      <td>{{$case->created_at->toDateString()}}</td>
                      <td>{{$case->synchronized_at->toDateString()}}</td>
                      @if ($case->forceClosed )
                      <td>Yes</td>
                      @else
                      <td>No</td>
                      @endif
                    </tr>
                @endforeach
                </table>
          @else
          <h2>There are No Cases Sent To Medal-Data Yet</h2>
          @endif
            </div>
          <div id="Cases_Not_Sent" class="tabcontent">
          @if(count($cases_not_sent)>0)
            <h2>CASES NOT SENT</h2>
                <table>
                    <tr>
                      <th>Sn</th>
                      <th>Patient Id</th>
                      <th>Consultation Date</th>
                      <th>Stage</th>
                      <th>Status</th>
                    </tr>
                    @foreach($cases_not_sent as $case)
                    <tr>
                      <td scope="row">{{ $loop->index+1 }}</td>
                      <td>{{$case->patient->uuid}}</td>
                      <td>{{$case->created_at->toDateString()}}</td>
                      @if($case->last_activity)
                      <td>{{$case->last_activity->stage}}</td>
                      @else
                      <td>No Activity yet</td>
                      @endif
                      @if($case->status)
                      <td>Closed</td>
                      @else
                      <td>Open</td>
                      @endif
                    </tr>
                    @endforeach
                </table>
          @else
          <h2>There are No cases Available</h2>
          @endif
          </div>

        <script>
            function openCity(evt, cityName) {
              var i, tabcontent, tablinks;
              tabcontent = document.getElementsByClassName("tabcontent");
              for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
              }
              tablinks = document.getElementsByClassName("tablinks");
              for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
              }
              document.getElementById(cityName).style.display = "block";
              evt.currentTarget.className += " active";
            }
            // Get the element with id="defaultOpen" and click on it
            document.getElementById("defaultOpen").click();
        </script>
    </body>

</html>



