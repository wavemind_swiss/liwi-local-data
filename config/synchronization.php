<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Offline Mode
    |--------------------------------------------------------------------------
    |
    | If this value is set to true, the application will attempt to synchronize
    | medical cases with the main data server as soon as an internet connection
    | is available. If it is set to true, the application will attempt to
    | synchronize at a fixed time everyday
    |
    */

    'offline_mode' => boolval(env('OFFLINE_MODE')),

    /*
    |--------------------------------------------------------------------------
    | Sync Domain
    |--------------------------------------------------------------------------
    |
    | This value is the domain name that the application will attempt to ping
    | in order to check if an internet connection is available
    |
    */

    'sync_domain' => env('SYNC_DOMAIN'),

    /*
    |--------------------------------------------------------------------------
    | Sync URL
    |--------------------------------------------------------------------------
    |
    | This value is the URL to which the data will be send to
    |
    */

    'sync_url' => env('SYNC_URL'),
    'auto_diag_url'=>env('AUTO_DIAG_URL'),

    /*
    |--------------------------------------------------------------------------
    | Daily Sync Time
    |--------------------------------------------------------------------------
    |
    | This value is the time at which the application will attempt to send
    | the data everyday. This value is ignored if offline mode is set.
    |
    */

    'daily_sync_version' => env('DAILY_SYNC_VERSION'),
    'daily_sync_time' => env('DAILY_SYNC_TIME'),
    'daily_close_time' => env('DAILY_CLOSE_TIME'),
    'daily_get_version_time' => env('DAILY_GET_VERSION_TIME'),

    /*
    |--------------------------------------------------------------------------
    | Offline mode out-of-sync threshold
    |--------------------------------------------------------------------------
    |
    | This value is the number of hours after which the cases are considered
    | out-of-sync (starting from creation time). Synchronization will be triggered
    | as soon as any closed case is out-of-sync and connection is available. The
    | value for online mode should to be much higher than the one for offline
    | mode because a reliable connection is expected to be available in online mode.
    | For this reason, cases in online mode only become out-of-sync when the daily
    | scheduled synchronization cannot take place for some reason.
    |
    */
    'offline_outofsync_thr' => intval(env('OFFLINE_OUTOFSYNC_THR')),
    'online_outofsync_thr' => intval(env('ONLINE_OUTOFSYNC_THR')),
    'cases_per_zip' => 5,
];
