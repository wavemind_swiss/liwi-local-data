<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Laravel\Socialite\Contracts\Factory;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $socialite = $this->app->make(Factory::class);
        $socialite->extend(
            'medaldata',
            function ($app) use ($socialite) {
                $config = $app['config']['services.medaldata'];

                return $socialite->buildProvider(MedalDataSocialiteProvider::class, $config);
            }
        );
    }
}
