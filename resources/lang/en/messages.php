<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'success' => 'Data successful created',
    'not_found' => 'Record not found',
    'unlock_success' => 'Unlock success',
    'lock_success' => 'Lock success',
    'medical_case_available' => 'Medical case available',
    'synchronize_success' => 'Synchronize success',
    'error_occured' => 'An error occured',
    'new_medical_case_saved' => 'New medical case saved',

    '401' => 'Unauthorized',
    '403' => 'Forbidden',
    '404' => 'Not Found',
    '405' => 'Method Not Allowed',
    '429' => 'Too many requests',
    '500' => 'Whoops, looks like something went wrong',

];
