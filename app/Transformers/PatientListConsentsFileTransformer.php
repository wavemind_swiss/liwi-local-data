<?php

namespace App\Transformers;

use App\Patient;
use App\Version;
use DateTime;
use League\Fractal\TransformerAbstract;

class PatientListConsentsFileTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include.
     *
     * @var array
     */
    protected $defaultIncludes = [

    ];

    /**
     * List of resources possible to include.
     *
     * @var array
     */
    protected $availableIncludes = [
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Patient $patient)
    {
        $medicalCase = $patient->medicalCases->last();

        $medicalCaseJSON = json_decode($medicalCase->case);
        $patientHash = [
            'id' => $patient->uuid,
            'consent_file' => $patient->consent_file,
            'values' => [],
        ];

        $algorithm_json = json_decode(Version::get_current_version()->json);

        foreach ($algorithm_json->mobile_config->patient_list as $nodeId) {
            $node = $medicalCaseJSON->nodes->$nodeId;
            $node_from_algo = $algorithm_json->nodes->$nodeId;
            if ($node_from_algo->display_format == 'Date') {
                $date = new DateTime($node->value);
                $value = $date->format('d/m/Y');
            } else {
                $value = $node->value;
            }
            array_push($patientHash['values'], $value);
        }

        return $patientHash;
    }
}
