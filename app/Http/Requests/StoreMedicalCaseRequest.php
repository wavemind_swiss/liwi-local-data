<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMedicalCaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'medical_case.id' => 'required|unique:medical_cases,uuid',
            'medical_case.json' => 'required',
            'medical_case.advancement' => 'required',
            'medical_case.advancement.stage' => 'required',
            'medical_case.advancement.step' => 'required',
            'medical_case.closedAt' => 'required',
            'medical_case.version_id' => 'required',
            'medical_case.createdAt' => 'required',
        ];
    }
}
