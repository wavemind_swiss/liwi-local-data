<?php

namespace App\Http\Controllers;

use App\Serializers\DataSerializer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller as BaseController;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function setResponseData($default, $data, $transformer, $includes = null)
    {
        if ($default) {
            $resource = fractal($data, $transformer);

            if ($includes) {
                $resource->parseIncludes($includes);
            }

            return $resource;
        }

        $resource = null;
        if ($data instanceof LengthAwarePaginator) {
            $dataCollection = $data->getCollection();
            $resource = new Collection($dataCollection, $transformer, '');
            $resource->setPaginator(new IlluminatePaginatorAdapter($data));
        } elseif ($data instanceof \Illuminate\Database\Eloquent\Collection) {
            $resource = new Collection($data, $transformer, '');
        } elseif ($data instanceof Model) {
            $resource = new Item($data, $transformer, '');
        } else {
            return [];
        }

        $manager = new Manager();
        $manager->setSerializer(new DataSerializer());

        if ($includes) {
            $manager->parseIncludes($includes);
        }

        $content = [];
        if ($resource) {
            $content = $manager->createData($resource)->toArray();
        }

        return $content;
    }
}
