<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Http\Requests\FindByPatientRequest;
use App\Http\Requests\PatientValuesRequest;
use App\Http\Requests\StorePatientRequest;
use App\Http\Requests\SyncronizePatientRequest;
use App\Http\Requests\UpdatePatientRequest;
use App\Http\Resources\CompletPatientResource;
use App\Http\Resources\PatientResource;
use App\MedicalCase;
use App\Patient;
use App\PatientValue;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PatientController extends Controller
{
    public function index(Request $request)
    {
        $params = $request->get('params');
        $terms = null;
        if ($params != null) {
            $params = json_decode($params, true);
            if (array_key_exists('terms', $params)) {
                $terms = $params['terms'];
            }
        }
        try {
            return PatientResource::collection(
                Patient::where('first_name', 'ILIKE', '%' . $terms . '%')
                    ->orWhere('last_name', 'ILIKE', '%' . $terms . '%')
                    ->orderBy('updated_at', 'desc')
                    ->paginate(15)
            );
        } catch (QueryException $ex) {
            return response()->json($ex, 422);
        }
    }

    public function consentsFile(Request $request)
    {
        try {
            return PatientResource::collection(Patient::orderBy('updated_at', 'desc')->paginate(15));
        } catch (QueryException $ex) {
            return response()->json($ex, 422);
        }
    }

    public function store(StorePatientRequest $request)
    {
        date_default_timezone_set('Europe/London');
        try {
            $birthdate = date('Y-m-d H:i:s', $request->patient['birth_date'] / 1000);
            $createdAt = date('Y-m-d H:i:s', $request->medical_case['createdAt'] / 1000);
            $patient = Patient::create([
                'uuid' => $request->patient['id'],
                'stickers_uuid' => $request->patient['uid'],
                'first_name' => $request->patient['first_name'],
                'last_name' => $request->patient['last_name'],
                'birth_date' => Carbon::createFromFormat('Y-m-d H:i:s', $birthdate),
                'birth_date_estimated' => $request->patient['birth_date_estimated'],
                'birth_date_estimated_type' => $request->patient['birth_date_estimated_type'],
                'consent' => $request->patient['consent'],
                'consent_file' => $request->patient['consent_file'] ?? null,
                'group_id' => $request->patient['group_id'],
                'other_group_id' => $request->patient['other_group_id'],
                'other_study_id' => $request->patient['other_study_id'],
                'other_uid' => $request->patient['other_uid'],
                'reason' => $request->patient['reason'],
                'study_id' => $request->patient['study_id'],
                'fail_safe' => $request->patient['fail_safe'] ?? false,
                'created_at' => Carbon::createFromFormat('Y-m-d H:i:s', $createdAt),
            ]);
        } catch (QueryException $ex) {
            return response()->json($ex, 422);
        }

        if ($patient) {
            $closedAt = date('Y-m-d H:i:s', $request->medical_case['closedAt'] / 1000);
            MedicalCase::create([
                'uuid' => $request->medical_case['id'],
                'fail_safe' => $request->medical_case['fail_safe'] ?? false,
                'version_id' => $request->medical_case['version_id'],
                'status' => ($request->medical_case['closedAt'] == 0) ? '' : 'close',
                'closed_at' => ($request->medical_case['closedAt'] == 0) ? null : Carbon::createFromFormat('Y-m-d H:i:s', $closedAt),
                'created_at' => Carbon::createFromFormat('Y-m-d H:i:s', $createdAt),
                'updated_at' => Carbon::createFromFormat('Y-m-d H:i:s', $createdAt),
                'json' => json_encode($request->medical_case['json']),
                'patient_id' => $patient->id,
                'stage' => $request->medical_case['advancement']['stage'],
                'step' => $request->medical_case['advancement']['step'],
            ]);

            return response()->json(trans('messages.success'), 200);
        }

        return response()->json(trans('messages.not_found'), 404);
    }

    public function update($uuid, UpdatePatientRequest $request)
    {
        date_default_timezone_set('Europe/London');
        $fields = $request->fields;
        $patient = Patient::firstWhere('uuid', $uuid);
        if ($patient) {
            try {
                foreach ($fields as $field) {
                    $name = $field['name'];
                    $value = $field['value'];
                    if ($name == 'birth_date') {
                        $value = date('Y-m-d H:i:s', $value / 1000);
                        $value = Carbon::createFromFormat('Y-m-d H:i:s', $value);
                    }
                    $patient->{$name} = $value;
                }
                $patient->save();
            } catch (QueryException $ex) {
                return response()->json($ex, 422);
            }

            return response()->json('patient updated', 200);
        }

        return response()->json(trans('messages.not_found'), 404);
    }

    public function findBy(FindByPatientRequest $request)
    {
        if (empty($request->field) || $request->field == 'uid') {
            $field = 'stickers_uuid';
        } elseif ($request->field == 'id') {
            $field = 'uuid';
        } else {
            $field = $request->field;
        }

        try {
            $patient = Patient::where($field, $request->value)->first();
        } catch (QueryException $ex) {
            return response()->json($ex, 422);
        }
        if ($patient) {
            return new CompletPatientResource($patient);
        }

        return response()->json(trans('messages.not_found'), 404);
    }

    public function storePatientValues($uuid, PatientValuesRequest $request)
    {
        $patient = Patient::firstWhere('uuid', $uuid);
        if ($patient) {
            try {
                foreach ($request->patient_values as $patientValue) {
                    $existPatientValue = PatientValue::where('patient_id', $patient->id)->where('node_id', $patientValue['node_id'])->first();
                    if ($existPatientValue != null) {
                        $existPatientValue->answer_id = $patientValue['answer_id'];
                        $existPatientValue->value = $patientValue['value'];
                        $existPatientValue->save();
                    } else {
                        PatientValue::create([
                            'patient_id' => $patient->id,
                            'node_id' => $patientValue['node_id'],
                            'answer_id' => $patientValue['answer_id'],
                            'value' => $patientValue['value'],
                        ]);
                    }
                }
            } catch (QueryException $ex) {
                return response()->json($ex, 422);
            }
        } else {
            return response()->json((new ModelNotFoundException())->setModel(Patient::class)->getMessage(), 404);
        }

        return response()->json('Patient values created', 200);
    }

    public function synchronize(SyncronizePatientRequest $request)
    {
        date_default_timezone_set('Europe/London');
        try {
            $birthdate = date('Y-m-d H:i:s', $request->patient['birth_date'] / 1000);
            $patient = Patient::updateOrCreate([
                'uuid' => $request->patient['id'],
            ], [
                'stickers_uuid' => $request->patient['uid'],
                'fail_safe' => $request->patient['fail_safe'] ?? false,
                'first_name' => $request->patient['first_name'],
                'last_name' => $request->patient['last_name'],
                'birth_date' => Carbon::createFromFormat('Y-m-d H:i:s', $birthdate),
                'birth_date_estimated' => $request->patient['birth_date_estimated'],
                'birth_date_estimated_type' => $request->patient['birth_date_estimated_type'],
                'study_id' => $request->patient['study_id'],
                'group_id' => $request->patient['group_id'],
                'other_uid' => $request->patient['other_uid'],
                'other_group_id' => $request->patient['other_group_id'],
                'reason' => $request->patient['reason'],
                'consent' => $request->patient['consent'],
                'consent_file' => $request->patient['consent_file'],
            ]);

            $medicalCases = $request->patient['medical_cases'];
            foreach ($medicalCases as $medicalCase) {
                $closedAt = date('Y-m-d H:i:s', $medicalCase['closedAt'] / 1000);
                $createdAt = date('Y-m-d H:i:s', $medicalCase['createdAt'] / 1000);
                $internal_medical_case = MedicalCase::updateOrCreate([
                    'uuid' => $medicalCase['id'],
                ], [
                    'device_id' => null,
                    'clinician' => $request->header("clinician") ?? null,
                    'fail_safe' => $medicalCase['fail_safe'],
                    'patient_id' => $patient->id,
                    'json' => json_encode($medicalCase['json']),
                    'version_id' => $medicalCase['version_id'],
                    'stage' => $medicalCase['advancement']['stage'],
                    'step' => $medicalCase['advancement']['step'],
                    'status' => ($medicalCase['closedAt'] == 0) ? '' : 'close',
                    'closed_at' => ($medicalCase['closedAt'] == 0) ? null : Carbon::createFromFormat('Y-m-d H:i:s', $closedAt),
                    'created_at' => Carbon::createFromFormat('Y-m-d H:i:s', $createdAt),
                    'updated_at' => Carbon::createFromFormat('Y-m-d H:i:s', $createdAt),
                ]);

                $activities = $medicalCase['activities'] ?? null;
                if ($activities) {
                    foreach ($activities as $activity) {
                        $createdAtActivity = array_key_exists('created_at', $activity) ?
                            date('Y-m-d H:i:s', $activity['created_at'] / 1000) :
                            now()->format('Y-m-d H:i:s');

                        Activity::create([
                            'uid' => Str::uuid(),
                            'fail_safe' => $activity['fail_safe'] ?? false,
                            'stage' => $activity['step'],
                            'clinician' => $activity['clinician'],
                            'nodes' => json_encode($activity['nodes']),
                            'mode' => $activity['mode'] ?? '',
                            'device_id' => $activity['device_id'],
                            'medical_case_id' => $internal_medical_case->id,
                            'created_at' => Carbon::createFromFormat('Y-m-d H:i:s', $createdAtActivity),
                        ]);
                    }
                }
            }

            $patientValues = $request->patient['patient_values'];
            foreach ($patientValues as $patientValue) {
                $existPatientValue = PatientValue::where('patient_id', $patient->id)->where('node_id', $patientValue['node_id'])->first();
                if ($existPatientValue == null) {
                    PatientValue::create([
                        'patient_id' => $patient->id,
                        'node_id' => $patientValue['node_id'],
                        'answer_id' => $patientValue['answer_id'],
                        'value' => $patientValue['value'],
                    ]);
                } else {
                    $existPatientValue->patient_id = $patient->id;
                    $existPatientValue->node_id = $patientValue['node_id'];
                    $existPatientValue->answer_id = $patientValue['answer_id'];
                    $existPatientValue->value = $patientValue['value'];
                    $existPatientValue->save();
                }
            }

            return response()->json(trans('messages.synchronize_success'), 200);
        } catch (QueryException $ex) {
            return response()->json($ex, 422);
        }
    }
}
