<?php

namespace App\Classes;

use Exception;

/**
 * Class LogViewer.
 */
class LogViewer
{
    /**
     * @var string file
     */
    private static $file;

    /**
     * Map debug levels to Bootstrap classes.
     *
     * @var array
     */
    private static $levels_classes = [
        'debug' => 'info',
        'info' => 'info',
        'notice' => 'info',
        'warning' => 'warning',
        'error' => 'danger',
        'critical' => 'danger',
        'alert' => 'danger',
        'emergency' => 'danger',
        'processed' => 'info',
    ];

    /**
     * Map debug levels to icon classes.
     *
     * @var array
     */
    private static $levels_imgs = [
        'debug' => 'info',
        'info' => 'info',
        'notice' => 'info',
        'warning' => 'warning',
        'error' => 'warning',
        'critical' => 'warning',
        'alert' => 'warning',
        'emergency' => 'warning',
        'processed' => 'info',
    ];

    /**
     * Log levels that are used.
     *
     * @var array
     */
    private static $log_levels = [
        'emergency',
        'alert',
        'critical',
        'error',
        // 'warning',
        // 'notice',
        // 'info',
        // 'debug',
        // 'processed',
    ];

    /**
     * Arbitrary max file size.
     */
    const MAX_FILE_SIZE = 52428800;

    /**
     * @throws Exception
     *
     * @return array
     */
    public static function all()
    {
        $log = [];

        if (!static::$file) {
            $log_file = static::getFiles();

            if (!count($log_file)) {
                throw new Exception('No log file found');
            }

            static::$file = $log_file[0]['file_name'];
        }

        if (app('files')->size(static::$file) > static::MAX_FILE_SIZE) {
            if (count($log_file) > 1) {
                static::$file = $log_file[1]['file_name'];

                if (app('files')->size(static::$file) > static::MAX_FILE_SIZE) {
                    throw new Exception('Log file too big to be processed');
                }
            }
            throw new Exception('Log file too big to be processed');
        }

        $file = app('files')->get(static::$file);

        $pattern = '/\[\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\].*/';

        preg_match_all($pattern, $file, $headings);

        if (!is_array($headings)) {
            throw new Exception('Corrupted log file');
        }

        $stack_trace = preg_split($pattern, $file);

        if ($stack_trace[0] < 1) {
            array_shift($stack_trace);
        }

        foreach ($headings as $h) {
            for ($i = count($h) - 1; $i > 0; $i--) {
                foreach (static::$log_levels as $level) {
                    if (strpos(strtolower($h[$i]), '.' . $level) || strpos(strtolower($h[$i]), $level . ':')) {
                        $pattern = '/^\[(?P<date>(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}))\](?:.*?(?P<context>(\w+))\.|.*?)' . $level . ': (?P<text>.*?)(?P<in_file> in .*?:[0-9]+)?$/i';
                        preg_match($pattern, $h[$i], $current);

                        if (!isset($current['text']) || in_array($current['text'], array_column($log, 'text'), true)) {
                            continue;
                        }

                        $log[] = [
                            'context' => $current['context'],
                            'level' => $level,
                            'level_class' => static::$levels_classes[$level],
                            'level_img' => static::$levels_imgs[$level],
                            'date' => $current['date'],
                            'text' => $current['text'],
                            'stack' => preg_replace("/^\n*/", '', $stack_trace[$i]),
                        ];
                    }
                }
            }
        }

        if (empty($log)) {
            throw new Exception('No error found in logs');
        }

        //Only return the n last new errors (n = logging.auto_diag.linecount)
        return array_slice($log, 0, config('logging.auto_diag.linecount'));
    }

    /**
     *
     * @return array
     */
    public static function getFiles()
    {
        $files = glob(storage_path() . '/logs/*.log');
        $files = array_reverse($files);
        $files = array_filter($files, 'is_file');

        if (is_array($files)) {
            foreach ($files as $k => $file) {
                $file_name = basename($file);
                if (app('files')->exists('storage/logs/' . $file_name)) {
                    $files[$k] = [
                        'file_name' => $file,
                        'last_modified' => app('files')->lastModified('storage/logs/' . $file_name),
                    ];
                }
            }

            usort($files, function ($a, $b) {
                return $b['last_modified'] - $a['last_modified'];
            });
        }

        return array_values($files);
    }
}
