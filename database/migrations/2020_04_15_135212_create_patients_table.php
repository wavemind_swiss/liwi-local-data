<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->boolean('fail_safe');
            $table->uuid('uid')->nullable()->unique();
            $table->string('study_id')->nullable();
            $table->integer('group_id')->nullable();
            $table->uuid('other_uid')->nullable();
            $table->string('other_study_id')->nullable();
            $table->integer('other_group_id')->nullable();
            $table->date('synchronized_at')->nullable();
            $table->string('reason')->nullable();
            $table->longText('consent_file')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
