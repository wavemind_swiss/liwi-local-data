<?php

namespace App\Http\Controllers;

use App\OauthToken;
use Exception;
use Laravel\Socialite\Facades\Socialite;

class EnrollementController extends Controller
{
    public function index()
    {
        if (!config('services.medaldata.client_id')) {
            return view('enroll.index', [
                'error' => 'Please set the client_id',
            ]);
        }

        $is_enrolled = OauthToken::where('client_id', config('services.medaldata.client_id'))->exists();

        return view('enroll.index', [
            'is_enrolled' => $is_enrolled,
        ]);
    }

    public function redirectToMedalData()
    {
        if (!config('services.medaldata.client_id')) {
            return redirect()->back();
        }

        $oauth_token = OauthToken::where('client_id', config('services.medaldata.client_id'))->first();

        if (!$oauth_token || !$oauth_token->access_token || !$oauth_token->refresh_token) {
            return Socialite::driver('medaldata')->redirect();
        }

        //If the token is valid, we don't need to ask for a new one
        if (Socialite::driver('medaldata')->isTokenValid($oauth_token)) {
            return redirect()->back();
        }

        //If the device is already enrolled, we refresh the token if needed
        if ($oauth_token->hasExpired()) {
            try {
                Socialite::driver('medaldata')->refreshToken($oauth_token);
            } catch (Exception $e) {
                //Refresh token is also expired
                if ($e->getCode() === 401) {
                    return Socialite::driver('medaldata')->redirect();
                }
                report($e);

                return view('enroll.index', [
                    'error' => 'An error occured',
                ]);
            }
        }

        return redirect()->back();
    }

    public function medalDataCallback()
    {
        try {
            Socialite::driver('medaldata')->getAccessToken();
        } catch (Exception $e) {
            report($e);

            return view('enroll.index', [
                'error' => 'An error occured',
            ]);
        }

        return redirect()->route('enroll.index');
    }
}
