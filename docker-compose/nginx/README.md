# Upgrading Nginx to HTTPS 

In this update, I have created a self-signed CA and then issued a certificate to be used by the server (the certificate is valid for one month and expires on May 21 2021 12:56:00) 

Below, I explain how the certificate can be renewed/recreated and how to change the duration of the certificates

## New Contents in the `nginx/` Directory

`srv.crt` certificate used by the server for HTTPS

`srv.key` private key used by the server for HTTPS

`ca.crt` Root CA certificate which should be stored by clients that want to access the server

## Modification of Nginx configuration

```nginx
server {
    listen 443 ssl;
    ssl_certificate conf.d/srv.crt;
    ssl_certificate_key conf.d/srv.key;
   # listen 80;
...
```

Instead of listening on port 80, the nginx server now only accepts https requests on port 443

the next lines point the nginx server to the server's certificate and private key

In accordance to all of this the `docker-compose.yml` file is updated at the root of the repository:

```nginx
nginx:
    image: nginx:alpine
    container_name: localdata-nginx
    restart: unless-stopped
    ports:
      - 8000:443
    volumes:
      - ./:/var/www:rw
      - ./docker-compose/nginx:/etc/nginx/conf.d/
    networks:
      - localdata
```

Now the port forwarding is done from port 8000 to port 443 instead of 80

## Testing the configuration

Run `docker-compose up -d` and when the server is running try to `curl` the server through https

```bash
$ curl https://0.0.0.0:8000
curl: (60) SSL certificate problem: unable to get local issuer certificate
More details here: https://curl.haxx.se/docs/sslcerts.html

curl failed to verify the legitimacy of the server and therefore could not
establish a secure connection to it. To learn more about this situation and
how to fix it, please visit the web page mentioned above.
```



it should fail because the system does not store the CA's certificate

you can specific the certificate with `--cacert` in order to trust the CA and connect to the server

navigate to `docker-compose/nginx/` and then type:

```bash
curl --cacert ca.crt https://0.0.0.0:8000
```

now it should connect via https without any errors.

In order to do the same from the medAL-reader android app you should follow the instructions in this link:

```
https://developer.android.com/training/articles/security-config#TrustingAdditionalCas
```

Be careful to include as well the system CAs, otherwise the readers will not connect to the creator anymore.