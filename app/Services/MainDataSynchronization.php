<?php

namespace App\Services;

use App\Http\Resources\MedalDataPatientResource;
use App\MedicalCase;
use App\OauthToken;
use App\Patient;
use DateInterval;
use DateTimeImmutable;
use Exception;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Laravel\Socialite\Facades\Socialite;
use ZipArchive;

class MainDataSynchronization
{
    /**
     * Attempts to send the cases to main data without checking any particular
     * condition (except that there must be cases to sync). This function
     * is typically meant to be called every day at a given time.
     */
    public static function attemptDailySync()
    {
        if (!config('synchronization.offline_mode')) { // daily sync is useless in offline mode
            Log::info('Attempting daily synchronization...');

            if (self::getUnsyncedCases()->count() > 0) {
                self::sendCasesIfServerReachable();
            } else {
                Log::info('No cases to send!');
            }
        }
    }

    /**
     * Attempts to send the cases to main data if there are cases that are out-of-sync.
     * The out-of-sync threshold value depends on whether offline mode is set. This function
     * is typically meant to be called every minute.
     */
    public static function attemptImmediateSync()
    {
        Log::info('Attempting immediate sync for data with missing sync');
        $oldestDate = self::getOldestNonSyncedCase();
        $offline = config('synchronization.offline_mode');
        $outOfSyncThreshold = new DateInterval('PT' . config($offline ? 'synchronization.offline_outofsync_thr' : 'synchronization.online_outofsync_thr') . 'H');

        if ($oldestDate < (new DateTimeImmutable())->sub($outOfSyncThreshold)) {
            self::sendCasesIfServerReachable();
        } else {
            Log::info('There are no out-of-sync cases for now!');
        }
    }

    /**
     * Checks if the server is reachable.
     */
    public static function serverReachable()
    {
        try {
            $response = Http::get(config('synchronization.sync_domain') . '/login');

            return $response->successful();
        } catch (ConnectionException $e) {
            return false;
        }
    }

    /**
     * Gets the cases that have not be sent to main data yet.
     */
    public static function getUnsyncedCases(): Collection
    {
        return MedicalCase::where('status', '=', 'close')->where('synchronized_at', '=', null)->get();
    }

    /**
     * Gets the date and time at which the oldest non synced case was created.
     */
    public static function getOldestNonSyncedCase()
    {
        return new DateTimeImmutable(MedicalCase::where('status', '=', 'close')->where('synchronized_at', '=', null)->min('created_at'));
    }

    /**
     * Sends the cases under the condition that the server is reachable.
     */
    public static function sendCasesIfServerReachable()
    {
        if (self::serverReachable()) {
            self::sendCases();
        } else {
            Log::info('Aborting synchronization, server is not reachable');
        }
    }

    /**
     * Unconditionally sends the cases to main data.
     */
    public static function sendCases()
    {
        $cases = self::getUnsyncedCases();
        $filteredCases = $cases->filter(function ($case, $key) {
            $patient = Patient::find($case->patient_id);
            if (!$patient) {
                Log::error("Medical case $case->id has no patient($case->patient_id)");
                return false;
            }
            return $patient->consent == true;
        });

        $chunked_cases = $filteredCases->chunk(config('synchronization.cases_per_zip'));

        try {
            $gitVersion = shell_exec('git describe --always --tags');
        } catch (Exception $ex) {
            $gitVersion = 'unknow';
        }

        if ($filteredCases->count() > 0) {
            foreach ($chunked_cases as $casesPerZip) {
                Log::info('Sending ' . count($casesPerZip) . ' cases');
                $zip = new ZipArchive;
                $directoryName = 'medical_cases';
                $zipFileName = 'medical_cases.zip';

                Storage::makeDirectory('medical_cases');

                if ($zip->open(public_path($zipFileName), ZipArchive::CREATE) === true) {
                    foreach ($casesPerZip as $medicalCase) {
                        $patient = new MedalDataPatientResource(Patient::find($medicalCase->patient_id));

                        $data = json_decode($medicalCase->json);
                        $data->gitVersion = $gitVersion;
                        $data->id = $medicalCase->uuid;
                        // add timestamp
                        $data->createdAt = ($medicalCase->created_at) ? $medicalCase->created_at->getPreciseTimestamp(3) : null;
                        $data->updatedAt = ($medicalCase->updated_at) ? $medicalCase->updated_at->getPreciseTimestamp(3) : null;
                        $data->synchronizedAt = ($medicalCase->synchronizedAt) ? $medicalCase->synchronized_at->getPreciseTimestamp(3) : null;
                        //$data->json_version = $medicalCase->synchronized_at->getPreciseTimestamp(3);
                        $data->closedAt = ($medicalCase->closed_at) ? $medicalCase->closed_at->getPreciseTimestamp(3) : null;
                        $data->fail_safe = $medicalCase->fail_safe;
                        $data->forceClosed = $medicalCase->forceClosed;
                        $data->advancement = ['step' => $medicalCase->step, 'stage' => $medicalCase->stage];

                        // I'm pretty sure that I'm not supposed to "toJson" and the json_decode but that's the best way I found
                        // So far you can blame me if you want. I'm sorry
                        $data->patient = json_decode($patient->toJson());
                        $data->version_id = $medicalCase->version_id;

                        $activityKey = 0;
                        if (count($medicalCase->activities) != 0) {
                            foreach ($medicalCase->activities as $activity) {
                                $data->activities[$activityKey] = [
                                    'id' => $activity->uid,
                                    'step' => $activity->stage,
                                    'clinician' => $activity->clinician,
                                    'nodes' => json_decode($activity->nodes),
                                    'mac_address' => $activity->mac_address,
                                    'device_id' => $activity->device_id,
                                    'medical_case_id' => $medicalCase->uuid,
                                    'fail_safe' => $activity->fail_safe,
                                    'created_at' => $activity->created_at->getPreciseTimestamp(3),
                                ];
                                $activityKey++;
                            }
                        } else {
                            $data->activities = null;
                        }

                        Storage::put($directoryName . '/' . $medicalCase->uuid . '.json', json_encode($data));
                    }

                    $files = File::files(storage_path('app/' . $directoryName));
                    foreach ($files as $key => $value) {
                        $relativeNameInZipFile = basename($value);
                        $zip->addFile($value, $relativeNameInZipFile);
                    }

                    $zip->close();
                }

                $file = fopen(public_path($zipFileName), 'r');

                $file_is_sent = self::sendFile($file);

                // Delete files used for this action
                Storage::deleteDirectory('medical_cases');
                fclose($file);
                unlink(public_path($zipFileName));

                if ($file_is_sent) {
                    foreach ($filteredCases as $medicalCase) {
                        $medicalCase->synchronized_at = date('Y-m-d');
                        $medicalCase->save();
                    }
                    Log::info('Upload successful!');
                }
            }
        }
    }

    /**
     * Send the file using the protected route.
     *
     * @param  File $file
     * @return bool
     */
    public static function sendFile($file): bool
    {
        $oauth_token = OauthToken::where('client_id', Config::get('services.medaldata.client_id'))->first();

        if (!$oauth_token || !$oauth_token->access_token) {
            Log::info('Device not enrolled');

            return false;
        }

        if ($oauth_token->hasExpired()) {
            try {
                $oauth_token = Socialite::driver('medaldata')->refreshToken($oauth_token);
            } catch (Exception $e) {
                report($e);
                Log::info('Refresh token expired');

                return false;
            }
        }

        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $oauth_token->access_token
        ])
            ->attach('file', $file, 'file')
            ->post(config('synchronization.sync_url'));

        if ($response->failed()) {
            Log::info('Upload failed with HTTP response code ' . $response->status());

            return false;
        }

        return true;
    }
}
