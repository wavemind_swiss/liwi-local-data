<?php

namespace App\Transformers;

use App\MedicalCase;
use League\Fractal\TransformerAbstract;

class MedicalCaseTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include.
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include.
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(MedicalCase $medicalCase)
    {
        return [
            'id' => $medicalCase->uuid,
            'fail_safe' => $medicalCase->fail_safe,
            'created_at' => $medicalCase->created_at,
            'updated_at' => $medicalCase->updated_at,
            'status' => $medicalCase->status,
            'patient_id' => $medicalCase->patient->uuid,
            'clinician' => $medicalCase->clinician,
            'mac_address' => $medicalCase->mac_address,
            'json' => $medicalCase->case,
        ];
    }
}
